/*
 * a2dp-utils, https://cmeerw.org
 * Copyright (C) 2016-2020, Christof Meerwald
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <functional>
#include <iostream>

#include <giomm.h>
#include <glibmm.h>

#include "rtp.h"
#include "sbc-codec.h"

#include <fcntl.h>
#include <poll.h>
#include <signal.h>
#include <unistd.h>

#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>

#include <sbc/sbc.h>

namespace
{
  Glib::RefPtr<Glib::MainLoop> loop;

  struct SBC_Stream_Encoder
    : SBC_Encoder, std::enable_shared_from_this<SBC_Stream_Encoder>
  {
    SBC_Stream_Encoder(a2dp_sbc_t conf, int fd, int ifd,
	guint16 read_mtu, guint16 write_mtu)
      : SBC_Encoder(conf, read_mtu, write_mtu),
	buffer_(new unsigned char[mtu()]),
	fd_(fd),
	ifd_(ifd)
    {
      {
	const int flags = fcntl(fd, F_GETFL, 0);
	fcntl(fd, F_SETFL, flags | O_NONBLOCK);
      }

      {
	const int flags = fcntl(ifd, F_GETFL, 0);
	fcntl(0, F_SETFL, flags | O_NONBLOCK);
      }
    }

    ~SBC_Stream_Encoder()
    { }

    bool on_input_data()
    {
      const int readlen = read(ifd_, buffer_.get(), remaining());
      if (readlen > 0)
      {
	auto result = encode(buffer_.get(), readlen);
	if (result.second != 0u)
	{
	  int rc = write(fd_, result.first, result.second);
	  if (rc == result.second)
	  {
	    auto self(shared_from_this());
	    timeout_signal_ = Glib::signal_timeout().connect([self] () {
		  self->on_output_timeout();
		  return false;
		}, 5000u);
	    out_signal_ = Glib::signal_io().connect([self] (Glib::IOCondition io_condition) {
		  self->on_output_data();
		  return false;
		}, fd_, Glib::IO_OUT);
	  }
	  else
	  {
	    loop->quit();
	  }

	  // wait for output buffer to be able to accept more data
	  return false;
	}
	else
	{
	  // need more data before we can send something
	  return true;
	}
      }
      else if ((readlen == -1) && (errno == EAGAIN))
      {
	return true;
      }
      else
      {
	loop->quit();
	return false;
      }
    }

    void on_output_data()
    {
      auto self(shared_from_this());
      timeout_signal_.disconnect();
      Glib::signal_io().connect([self] (Glib::IOCondition io_condition) {
	  return self->on_input_data();
	}, 0, Glib::IO_IN | Glib::IO_HUP);
    }

    void on_output_timeout()
    {
      out_signal_.disconnect();
      loop->quit();
    }

  private:
    const std::unique_ptr<unsigned char []> buffer_;
    const int fd_;
    const int ifd_;
    sigc::connection timeout_signal_;
    sigc::connection out_signal_;
  };

  struct SBC_Stream_Decoder
    : SBC_Decoder, std::enable_shared_from_this<SBC_Stream_Decoder>
  {
    SBC_Stream_Decoder(a2dp_sbc_t conf, int fd, int ofd, guint16 read_mtu, guint16 write_mtu)
      : SBC_Decoder(conf, read_mtu, write_mtu),
	enc_bufsize_(read_mtu),
	enc_buf_(new unsigned char[enc_bufsize_]),
	fd_(fd),
	ofd_(ofd)
    { }

    ~SBC_Stream_Decoder()
    {
      close(ofd_);
    }

    bool on_input_data()
    {
      const int readlen = read(fd_, enc_buf_.get(), enc_bufsize_);
      if ((readlen == -1) && (errno == EAGAIN))
      {
	return true;
      }
      else if (readlen == 0)
      {
	return false;
      }
      else if (readlen < 0)
      {
	loop->quit();
	return false;
      }
      else
      {
	auto result = decode(enc_buf_.get(), readlen);

	if (result.second != 0u)
	{
	  int rc = write(ofd_, result.first, result.second);
	  if (rc == result.second)
	  {
	    auto self(shared_from_this());
	    Glib::signal_io().connect([self] (Glib::IOCondition io_condition) {
		  self->on_output_data();
		  return false;
		}, 1, Glib::IO_OUT);
	  }

	  // wait for output buffer to be able to accept more data
	  return false;
	}
	else
	{
	  // need more data before we can send something
	  return true;
	}
      }
    }

    void on_output_data()
    {
      auto self(shared_from_this());
      Glib::signal_io().connect([self] (Glib::IOCondition io_condition) {
	  return self->on_input_data();
	}, fd_, Glib::IO_IN | Glib::IO_HUP);
    }

  private:
    const guint16 enc_bufsize_;
    const std::unique_ptr<unsigned char[]> enc_buf_;

    const int fd_;
    const int ofd_;
  };

  void register_media_endpoint(Glib::RefPtr<Gio::DBus::Connection> const &conn,
			       std::string const &path, bool is_source)
  {
    a2dp_sbc_t capabilities;
    capabilities.channel_mode = SBC_CHANNEL_MODE_STEREO;
    capabilities.frequency = SBC_SAMPLING_FREQ_48000;
    capabilities.allocation_method = SBC_ALLOCATION_LOUDNESS;
    capabilities.subbands = SBC_SUBBANDS_8;
    capabilities.block_length = SBC_BLOCK_LENGTH_16;
    capabilities.min_bitpool = MIN_BITPOOL;
    capabilities.max_bitpool = MAX_BITPOOL;

    std::vector<Glib::VariantBase> args;
    Glib::Variant<Glib::ustring> obj_path;
    Glib::Variant<Glib::ustring>::create_object_path(obj_path, is_source ?
						     "/mediaendpoint/a2dpsource" :
						     "/mediaendpoint/a2dpsink");
    args.push_back(obj_path);

    std::map<Glib::ustring, Glib::VariantBase> props;
    props["UUID"] = Glib::Variant<Glib::ustring>::create(is_source ?
							 "0000110a-0000-1000-8000-00805f9b34fb" :
							 "0000110b-0000-1000-8000-00805f9b34fb");
    props["Codec"] = Glib::Variant<unsigned char>::create(A2DP_CODEC_SBC);
    props["Capabilities"] = Glib::Variant<std::vector<unsigned char> >::create(std::vector<unsigned char>(reinterpret_cast<unsigned char *>(&capabilities), reinterpret_cast<unsigned char *>(&capabilities + 1)));

    args.push_back(Glib::Variant<std::map<Glib::ustring, Glib::VariantBase>>::create(props));
    Glib::VariantContainerBase varArgs(Glib::Variant<std::vector<Glib::VariantBase> >::create_tuple(args));

    auto media_proxy(Gio::DBus::Proxy::create_sync
		     (conn,
		      "org.bluez",
		      path.c_str(),
		      "org.bluez.Media1",
		      Glib::RefPtr<Gio::DBus::InterfaceInfo>(),
		      Gio::DBus::ProxyFlags::PROXY_FLAGS_DO_NOT_LOAD_PROPERTIES));

    media_proxy->call_sync("RegisterEndpoint", varArgs);
  }

  void endpoint_select_configuration(const Glib::RefPtr<Gio::DBus::Connection> &conn,
				     const Glib::VariantContainerBase &parameters,
				     const Glib::RefPtr<Gio::DBus::MethodInvocation> &invocation)
  {
    Glib::VariantBase capsVariant;
    parameters.get_child(capsVariant, 0);

    auto capsData(Glib::VariantBase::cast_dynamic<Glib::Variant<std::vector<unsigned char> > >(capsVariant).get());
    a2dp_sbc_t caps = *reinterpret_cast<a2dp_sbc_t const *>(capsData.data());

    a2dp_sbc_t config = {};
    config.frequency = SBC_SAMPLING_FREQ_48000;
    config.channel_mode = SBC_CHANNEL_MODE_STEREO;
    config.block_length = SBC_BLOCK_LENGTH_16;
    config.subbands = SBC_SUBBANDS_8;
    config.allocation_method = SBC_ALLOCATION_LOUDNESS;

    config.min_bitpool = caps.min_bitpool;
    config.max_bitpool = caps.max_bitpool;

    std::vector<unsigned char> configData(sizeof(config), 0);
    *reinterpret_cast<a2dp_sbc_t *>(configData.data()) = config;

    Glib::VariantContainerBase result(Glib::Variant<std::vector<unsigned char> >::create_tuple(Glib::Variant<std::vector<unsigned char> >::create(configData)));

    invocation->return_value(result);
  }

  template<typename Stream_Class>
  std::shared_ptr<Stream_Class > endpoint_set_configuration(const Glib::RefPtr<Gio::DBus::Connection> &conn,
      const Glib::VariantContainerBase &parameters,
      const Glib::RefPtr<Gio::DBus::MethodInvocation> &invocation,
      std::function<int ()> const &get_fd)
  {
    Glib::VariantBase transportVariant;
    parameters.get_child(transportVariant, 0);
    auto transport(Glib::VariantBase::cast_dynamic<Glib::Variant<Glib::ustring> >(transportVariant).get());

    Glib::VariantBase propsVariant;
    parameters.get_child(propsVariant, 1);
    auto props(Glib::VariantBase::cast_dynamic<Glib::Variant<std::map<Glib::ustring, Glib::VariantBase> > >(propsVariant).get());

    invocation->return_value(Glib::VariantContainerBase());

    auto device_iter = props.find("Device");
    if (device_iter != props.end())
    {
      auto device (Glib::VariantBase::cast_dynamic<Glib::Variant<Glib::ustring> >(device_iter->second).get());

      auto conf_iter = props.find("Configuration");
      if (conf_iter != props.end())
      {
	auto const & confData (Glib::VariantBase::cast_dynamic<Glib::Variant<std::vector<unsigned char> > >(conf_iter->second).get());
	auto conf = *reinterpret_cast<const a2dp_sbc_t *>(confData.data());

	auto transport_proxy(Gio::DBus::Proxy::create_sync
			     (conn,
			      "org.bluez",
			      transport,
			      "org.bluez.MediaTransport1",
			      Glib::RefPtr<Gio::DBus::InterfaceInfo>(),
			      Gio::DBus::ProxyFlags::PROXY_FLAGS_DO_NOT_LOAD_PROPERTIES));

	Glib::RefPtr<Gio::UnixFDList> fd_list; 
	Glib::VariantContainerBase result(transport_proxy->call_sync("Acquire", Glib::VariantContainerBase(), Glib::RefPtr<Gio::UnixFDList>(), fd_list));

	auto read_mtu (Glib::VariantBase::cast_dynamic<Glib::Variant<guint16> >(result.get_child(1)).get());
	auto write_mtu (Glib::VariantBase::cast_dynamic<Glib::Variant<guint16> >(result.get_child(2)).get());

	auto fd = fd_list->get(0);
	auto s(std::make_shared<Stream_Class>(conf, fd, get_fd(), read_mtu, write_mtu));

	return s;
      }
    }

    return { };
  }

  void endpoint_clear_configuration(const Glib::RefPtr<Gio::DBus::Connection> &conn,
				    const Glib::VariantContainerBase &parameters,
				    const Glib::RefPtr<Gio::DBus::MethodInvocation> &invocation)
  {
  }

  void endpoint_release(const Glib::RefPtr<Gio::DBus::Connection> &conn,
			const Glib::VariantContainerBase &parameters,
			const Glib::RefPtr<Gio::DBus::MethodInvocation> &invocation)
  {
  }
}

int main(int argc, char *argv[])
{
  {
    struct sigaction act { };
    act.sa_handler = SIG_IGN;
    act.sa_flags = SA_NOCLDWAIT;
    sigaction(SIGCHLD, &act, nullptr);
  }

  {
    struct sigaction act { };
    act.sa_handler = SIG_IGN;
    sigaction(SIGPIPE, &act, nullptr);
  }

  Gio::init();
  loop = Glib::MainLoop::create();

  const bool has_ctrl_arg = (argc > 1) && (argv[1][0] != '-');

  Glib::RefPtr<Gio::DBus::Connection> conn(Gio::DBus::Connection::get_sync(Gio::DBus::BusType::BUS_TYPE_SYSTEM));
  std::string const &path(std::string("/org/bluez/") +
      (has_ctrl_arg ? argv[1] : "hci0"));

  const bool has_dev_arg = has_ctrl_arg && (argc > 2) && (argv[2][0] != '-');
  if (has_dev_arg)
  {
    Glib::ustring introspection_xml =
      R"(<node name="/mediaendpoint/a2dpsource">
  <interface name="org.bluez.MediaEndpoint1">
    <method name="Release"></method>
    <method name="ClearConfiguration"><arg name="transport" type="o" direction="in"/></method>
    <method name="SelectConfiguration"><arg name="caps" type="ay" direction="in"/><arg name="caps" type="ay" direction="out"/></method>
    <method name="SetConfiguration"><arg name="transport" type="o" direction="in"/><arg name="properties" type="a{sv}" direction="in"/></method>
  </interface>
</node>)";

    Glib::RefPtr<Gio::DBus::NodeInfo> introspection_data =
      Gio::DBus::NodeInfo::create_for_xml(introspection_xml);

    Gio::DBus::InterfaceVTable interface_table([] (const Glib::RefPtr<Gio::DBus::Connection> &conn,
	    const Glib::ustring &sender,
	    const Glib::ustring &object_path,
	    const Glib::ustring &interface_name,
	    const Glib::ustring &method_name,
	    const Glib::VariantContainerBase &parameters,
	    const Glib::RefPtr<Gio::DBus::MethodInvocation> &invocation) {
	  static std::shared_ptr<SBC_Stream_Encoder> stream;

	  if (method_name == "SelectConfiguration")
	  {
	    endpoint_select_configuration(conn, parameters, invocation);
	  }
	  else if (method_name == "SetConfiguration")
	  {
	    stream = endpoint_set_configuration<SBC_Stream_Encoder>(conn, parameters, invocation, [] () { return 0; });
	    if (stream)
	    {
	      stream->on_output_data();
	    }
	  }
	  else if (method_name == "ClearConfiguration")
	  {
	    endpoint_clear_configuration(conn, parameters, invocation);
	    if (stream)
	    {
	      stream.reset();
	    }
	  }
	  else if (method_name == "Release")
	  {
	    endpoint_release(conn, parameters, invocation);
	  }
	});
    conn->register_object("/mediaendpoint/a2dpsource",
			  introspection_data->lookup_interface(),
			  interface_table);
    register_media_endpoint(conn, path, true /* source */);

    std::string const & dev(path + "/dev_" + argv[2]);

    loop->get_context()->signal_idle().connect_once([conn, dev] () {
	auto dev_proxy(Gio::DBus::Proxy::create_sync
		       (conn,
			"org.bluez",
			dev,
			"org.bluez.Device1",
			Glib::RefPtr<Gio::DBus::InterfaceInfo>(),
			Gio::DBus::ProxyFlags::PROXY_FLAGS_DO_NOT_LOAD_PROPERTIES));

	dev_proxy->call("Connect",
			[dev_proxy] (Glib::RefPtr<Gio::AsyncResult>& result) {
			  try
			  {
			    dev_proxy->call_finish(result);
			  }
			  catch (Glib::Exception const & exc)
			  {
			    std::cerr << "Connect failed: " << exc.what()
				      << '\n';
			    loop->quit();
			  }
			});
      });

    loop->run();
  }
  else
  {
    Glib::ustring introspection_xml =
      R"(<node name="/mediaendpoint/a2dpsink">
  <interface name="org.bluez.MediaEndpoint1">
    <method name="Release"></method>
    <method name="ClearConfiguration"><arg name="transport" type="o" direction="in"/></method>
    <method name="SelectConfiguration"><arg name="caps" type="ay" direction="in"/><arg name="caps" type="ay" direction="out"/></method>
    <method name="SetConfiguration"><arg name="transport" type="o" direction="in"/><arg name="properties" type="a{sv}" direction="in"/></method>
  </interface>
</node>)";

    Glib::RefPtr<Gio::DBus::NodeInfo> introspection_data =
      Gio::DBus::NodeInfo::create_for_xml(introspection_xml);

    char ** const cmd_argv = argv + (has_ctrl_arg ? 3 : 2);
    auto get_fd = [cmd_argv] () -> int {
      int pipefds[2];
      pipe(pipefds);

      pid_t cpid = fork();
      if (cpid == 0)
      {
	close(pipefds[1]);
	dup2(pipefds[0], 0);
	close(pipefds[0]);
	execvp(*cmd_argv, cmd_argv);
	return -1;
      }
      else if (cpid != -1)
      {
	close(pipefds[0]);
	pipefds[0] = -1;
      }
      else
      {
	perror("Unable to fork");

	close(pipefds[0]);
	close(pipefds[1]);
	pipefds[0] = pipefds[1] = -1;
      }

      return pipefds[1];
    };

    Gio::DBus::InterfaceVTable interface_table([get_fd] (const Glib::RefPtr<Gio::DBus::Connection> &conn,
	    const Glib::ustring &sender,
	    const Glib::ustring &object_path,
	    const Glib::ustring &interface_name,
	    const Glib::ustring &method_name,
	    const Glib::VariantContainerBase &parameters,
	    const Glib::RefPtr<Gio::DBus::MethodInvocation> &invocation) {
	  static std::shared_ptr<SBC_Stream_Decoder> stream;

	  if (method_name == "SelectConfiguration")
	  {
	    endpoint_select_configuration(conn, parameters, invocation);
	  }
	  else if (method_name == "SetConfiguration")
	  {
	    stream = endpoint_set_configuration<SBC_Stream_Decoder>(conn, parameters, invocation, get_fd);
	    if (stream)
	    {
	      stream->on_output_data();
	    }
	  }
	  else if (method_name == "ClearConfiguration")
	  {
	    endpoint_clear_configuration(conn, parameters, invocation);
	    if (stream)
	    {
	      stream.reset();
	    }
	  }
	  else if (method_name == "Release")
	  {
	    endpoint_release(conn, parameters, invocation);
	  }
	});
    conn->register_object("/mediaendpoint/a2dpsink",
			  introspection_data->lookup_interface(),
			  interface_table);
    register_media_endpoint(conn, path, false /* sink */);

    loop->run();
  }
}
