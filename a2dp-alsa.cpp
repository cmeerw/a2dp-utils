/*
 * a2dp-utils, https://cmeerw.org
 * Copyright (C) 2016-2019, Christof Meerwald
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

/*
 * Partly based on bluealsa-pcm.c
 * Copyright (c) 2016-2018 Arkadiusz Bokowy
 *
 * This file is a part of bluez-alsa.
 *
 * This project is licensed under the terms of the MIT license.
 *
 */

#define PIC 1

#include <errno.h>
#include <poll.h>
#include <stdlib.h>
#include <unistd.h>

#include <giomm.h>
#include <glibmm.h>

#include "sbc-codec.h"


#include <arpa/inet.h>
#include <sys/eventfd.h>
#include <sys/socket.h>
#include <sys/timerfd.h>
#include <sys/types.h>

#include <alsa/asoundlib.h>
#include <alsa/pcm_external.h>

#include <algorithm>
#include <atomic>
#include <condition_variable>
#include <iostream>
#include <iterator>
#include <mutex>
#include <thread>

#include <sbc/sbc.h>


namespace
{
  struct SBC_Stream_Encoder
    : SBC_Encoder
  {
    SBC_Stream_Encoder(a2dp_sbc_t conf, int fd,
        guint16 read_mtu, guint16 write_mtu)
      : SBC_Encoder(conf, read_mtu, write_mtu),
        fd_(fd)
    {
      const int flags = fcntl(fd, F_GETFL, 0);
      fcntl(fd, F_SETFL, flags & ~O_NONBLOCK);
    }

    void on_data(unsigned char *data, unsigned int len)
    {
      while (len != 0u)
      {
        const unsigned int l = std::min<unsigned int>(len, remaining());
        auto result = encode(data, l);
        len -= l;
        data += l;
        if (result.second != 0u)
        {
          write(fd_, result.first, result.second);
        }
      }
    }

  private:
    int fd_;
  };
}

struct a2dp_pcm
{
  snd_pcm_ioplug_t io;

  std::string interface;
  std::string device;

  int event_fd;
  std::atomic<snd_pcm_uframes_t> io_ptr;

  snd_pcm_uframes_t ts_ptr;
  struct timespec ts;

  std::unique_ptr<Glib::Dispatcher> start;
  std::unique_ptr<Glib::Dispatcher> stop;
  std::unique_ptr<Glib::Dispatcher> close;

  sigc::connection timer;

  std::mutex sync;
  std::condition_variable cond_var;

  std::shared_ptr<SBC_Stream_Encoder> encoder;
  std::thread t;
};

namespace
{
  void register_media_endpoint(Glib::RefPtr<Gio::DBus::Connection> const &conn,
			       std::string const &path, bool is_source)
  {
    a2dp_sbc_t capabilities;
    capabilities.channel_mode = SBC_CHANNEL_MODE_STEREO;
    capabilities.frequency = SBC_SAMPLING_FREQ_48000;
    capabilities.allocation_method = SBC_ALLOCATION_LOUDNESS;
    capabilities.subbands = SBC_SUBBANDS_8;
    capabilities.block_length = SBC_BLOCK_LENGTH_16;
    capabilities.min_bitpool = MIN_BITPOOL;
    capabilities.max_bitpool = MAX_BITPOOL;

    std::vector<Glib::VariantBase> args;
    Glib::Variant<Glib::ustring> obj_path;
    Glib::Variant<Glib::ustring>::create_object_path(obj_path, is_source ?
						     "/mediaendpoint/a2dpsource" :
						     "/mediaendpoint/a2dpsink");
    args.push_back(obj_path);

    std::map<Glib::ustring, Glib::VariantBase> props;
    props["UUID"] = Glib::Variant<Glib::ustring>::create(is_source ?
							 "0000110a-0000-1000-8000-00805f9b34fb" :
							 "0000110b-0000-1000-8000-00805f9b34fb");
    props["Codec"] = Glib::Variant<unsigned char>::create(A2DP_CODEC_SBC);
    props["Capabilities"] = Glib::Variant<std::vector<unsigned char> >::create(std::vector<unsigned char>(reinterpret_cast<unsigned char *>(&capabilities), reinterpret_cast<unsigned char *>(&capabilities + 1)));

    args.push_back(Glib::Variant<std::map<Glib::ustring, Glib::VariantBase>>::create(props));
    Glib::VariantContainerBase varArgs(Glib::Variant<std::vector<Glib::VariantBase> >::create_tuple(args));

    auto media_proxy(Gio::DBus::Proxy::create_sync
		     (conn,
		      "org.bluez",
		      path.c_str(),
		      "org.bluez.Media1",
		      Glib::RefPtr<Gio::DBus::InterfaceInfo>(),
		      Gio::DBus::ProxyFlags::PROXY_FLAGS_DO_NOT_LOAD_PROPERTIES));

    media_proxy->call_sync("RegisterEndpoint", varArgs);
  }

  void endpoint_select_configuration(const Glib::RefPtr<Gio::DBus::Connection> &conn,
				     const Glib::VariantContainerBase &parameters,
				     const Glib::RefPtr<Gio::DBus::MethodInvocation> &invocation)
  {
    Glib::VariantBase capsVariant;
    parameters.get_child(capsVariant, 0);

    auto capsData(Glib::VariantBase::cast_dynamic<Glib::Variant<std::vector<unsigned char> > >(capsVariant).get());
    a2dp_sbc_t caps = *reinterpret_cast<a2dp_sbc_t const *>(capsData.data());

    a2dp_sbc_t config = {};
    config.frequency = SBC_SAMPLING_FREQ_48000;
    config.channel_mode = SBC_CHANNEL_MODE_STEREO;
    config.block_length = SBC_BLOCK_LENGTH_16;
    config.subbands = SBC_SUBBANDS_8;
    config.allocation_method = SBC_ALLOCATION_LOUDNESS;

    config.min_bitpool = caps.min_bitpool;
    config.max_bitpool = caps.max_bitpool;

    std::vector<unsigned char> configData(sizeof(config), 0);
    *reinterpret_cast<a2dp_sbc_t *>(configData.data()) = config;

    Glib::VariantContainerBase result(Glib::Variant<std::vector<unsigned char> >::create_tuple(Glib::Variant<std::vector<unsigned char> >::create(configData)));

    invocation->return_value(result);
  }

  template<typename Stream_Class>
  std::shared_ptr<Stream_Class> endpoint_set_configuration(const Glib::RefPtr<Gio::DBus::Connection> &conn,
      const Glib::VariantContainerBase &parameters,
      const Glib::RefPtr<Gio::DBus::MethodInvocation> &invocation)
  {
    Glib::VariantBase transportVariant;
    parameters.get_child(transportVariant, 0);
    auto transport(Glib::VariantBase::cast_dynamic<Glib::Variant<Glib::ustring> >(transportVariant).get());

    Glib::VariantBase propsVariant;
    parameters.get_child(propsVariant, 1);
    auto props(Glib::VariantBase::cast_dynamic<Glib::Variant<std::map<Glib::ustring, Glib::VariantBase> > >(propsVariant).get());

    invocation->return_value(Glib::VariantContainerBase());

    auto device_iter = props.find("Device");
    if (device_iter != props.end())
    {
      auto device (Glib::VariantBase::cast_dynamic<Glib::Variant<Glib::ustring> >(device_iter->second).get());

      auto conf_iter = props.find("Configuration");
      if (conf_iter != props.end())
      {
	auto const & confData (Glib::VariantBase::cast_dynamic<Glib::Variant<std::vector<unsigned char> > >(conf_iter->second).get());
	auto conf = *reinterpret_cast<const a2dp_sbc_t *>(confData.data());

	auto transport_proxy(Gio::DBus::Proxy::create_sync
			     (conn,
			      "org.bluez",
			      transport,
			      "org.bluez.MediaTransport1",
			      Glib::RefPtr<Gio::DBus::InterfaceInfo>(),
			      Gio::DBus::ProxyFlags::PROXY_FLAGS_DO_NOT_LOAD_PROPERTIES));

	Glib::RefPtr<Gio::UnixFDList> fd_list;
	Glib::VariantContainerBase result(transport_proxy->call_sync("Acquire", Glib::VariantContainerBase(), Glib::RefPtr<Gio::UnixFDList>(), fd_list));

	auto read_mtu (Glib::VariantBase::cast_dynamic<Glib::Variant<guint16> >(result.get_child(1)).get());
	auto write_mtu (Glib::VariantBase::cast_dynamic<Glib::Variant<guint16> >(result.get_child(2)).get());

	auto fd = fd_list->get(0);
	auto s(std::make_shared<Stream_Class>(conf, fd, read_mtu, write_mtu));

        return s;
      }
    }

    return { };
  }

  void endpoint_clear_configuration(const Glib::RefPtr<Gio::DBus::Connection> &conn,
				    const Glib::VariantContainerBase &parameters,
				    const Glib::RefPtr<Gio::DBus::MethodInvocation> &invocation)
  {
  }

  void endpoint_release(const Glib::RefPtr<Gio::DBus::Connection> &conn,
			const Glib::VariantContainerBase &parameters,
			const Glib::RefPtr<Gio::DBus::MethodInvocation> &invocation)
  {
  }

  void consumer(a2dp_pcm *pcm)
  {
    snd_pcm_ioplug_t *io = &pcm->io;

    static Glib::RefPtr<Glib::MainLoop> loop (Glib::MainLoop::create());
    static Glib::RefPtr<Gio::DBus::Connection> conn(Gio::DBus::Connection::get_sync(Gio::DBus::BusType::BUS_TYPE_SYSTEM));

    const snd_pcm_channel_area_t *areas = snd_pcm_ioplug_mmap_areas(io);
    unsigned char *buffer = static_cast<unsigned char *>(areas->addr) + (areas->first * areas->step) / 8;

    const unsigned int buffer_periods = io->buffer_size / io->period_size;
    const unsigned int us = io->buffer_size * 1000000u / 2u / io->rate;

    pcm->start.reset(new Glib::Dispatcher(loop->get_context()));
    pcm->stop.reset(new Glib::Dispatcher(loop->get_context()));
    pcm->close.reset(new Glib::Dispatcher(loop->get_context()));

    auto timer_fn = [pcm, buffer, buffer_periods] () {
      snd_pcm_uframes_t appl_ptr = pcm->io.appl_ptr;

      struct timespec ts;
      clock_gettime(CLOCK_MONOTONIC, &ts);

      unsigned long long diff = ts.tv_sec * 1000ull + ts.tv_nsec / 1000000ull;
      diff -= pcm->ts.tv_sec * 1000ull + pcm->ts.tv_nsec / 1000000ull;

      snd_pcm_uframes_t to_ptr = ((diff * pcm->io.rate / 1000ull + pcm->io.period_size / 2u) / pcm->io.period_size) * pcm->io.period_size + buffer_periods * pcm->io.period_size + pcm->ts_ptr;
      if (to_ptr > appl_ptr)
      {
        pcm->ts_ptr = pcm->io_ptr;
        pcm->ts = ts;
        to_ptr = appl_ptr;
      }

      snd_pcm_uframes_t consume = to_ptr - pcm->io_ptr;
      if (consume > 0u)
      {
        if ((pcm->io_ptr + consume == appl_ptr) ||
            (consume == pcm->io.buffer_size))
        {
          consume -= pcm->io.period_size;
        }
      }

      if (consume > 0u)
      {
        size_t off = (pcm->io_ptr % pcm->io.buffer_size);
        size_t end = off + consume;

        if (end > pcm->io.buffer_size)
        {
          pcm->encoder->on_data(buffer + 4*off, 4*(pcm->io.buffer_size - off));
          pcm->encoder->on_data(buffer, 4*(end - pcm->io.buffer_size));
        }
        else if (consume != 0u)
        {
          pcm->encoder->on_data(buffer + 4*off, 4*(end - off));
        }

        pcm->io_ptr += consume;
      }

      eventfd_write(pcm->event_fd, 1);

      return true;
    };

    pcm->start->connect([us, pcm, &timer_fn] () {
      pcm->ts_ptr = 0;
      clock_gettime(CLOCK_MONOTONIC, &pcm->ts);

      pcm->timer = loop->get_context()->signal_timeout().connect(timer_fn, us / 1000);
      timer_fn();
    });
    pcm->stop->connect([pcm] () {
      pcm->timer.disconnect();
    });
    pcm->close->connect([] () {
      loop->quit();
    });

    std::string const &path(std::string("/org/bluez/") + pcm->interface);

    Glib::ustring introspection_xml =
      R"(<node name="/mediaendpoint/a2dpsource">
  <interface name="org.bluez.MediaEndpoint1">
    <method name="Release"></method>
    <method name="ClearConfiguration"><arg name="transport" type="o" direction="in"/></method>
    <method name="SelectConfiguration"><arg name="caps" type="ay" direction="in"/><arg name="caps" type="ay" direction="out"/></method>
    <method name="SetConfiguration"><arg name="transport" type="o" direction="in"/><arg name="properties" type="a{sv}" direction="in"/></method>
  </interface>
</node>)";

    Glib::RefPtr<Gio::DBus::NodeInfo> introspection_data =
      Gio::DBus::NodeInfo::create_for_xml(introspection_xml);

    Gio::DBus::InterfaceVTable interface_table([pcm] (const Glib::RefPtr<Gio::DBus::Connection> &conn,
            const Glib::ustring &sender,
            const Glib::ustring &object_path,
            const Glib::ustring &interface_name,
            const Glib::ustring &method_name,
            const Glib::VariantContainerBase &parameters,
            const Glib::RefPtr<Gio::DBus::MethodInvocation> &invocation) {
          if (method_name == "SelectConfiguration")
          {
            endpoint_select_configuration(conn, parameters, invocation);
          }
          else if (method_name == "SetConfiguration")
          {
            std::lock_guard<std::mutex> g(pcm->sync);
            pcm->encoder = endpoint_set_configuration<SBC_Stream_Encoder>(conn, parameters, invocation);
            pcm->cond_var.notify_one();
          }
          else if (method_name == "ClearConfiguration")
          {
            endpoint_clear_configuration(conn, parameters, invocation);
          }
          else if (method_name == "Release")
          {
            endpoint_release(conn, parameters, invocation);
          }
        });

    conn->register_object("/mediaendpoint/a2dpsource",
			  introspection_data->lookup_interface(),
			  interface_table);

    register_media_endpoint(conn, path, true /* source */);

    std::string dev(path + "/dev_");
    std::transform(pcm->device.begin(),
        pcm->device.end(),
        std::back_inserter(dev),
        [] (char c) {
          return c == ':' ? '_' : c;
        });

    loop->get_context()->signal_idle().connect_once([dev] () {
	auto dev_proxy(Gio::DBus::Proxy::create_sync
		       (conn,
			"org.bluez",
			dev,
			"org.bluez.Device1",
			Glib::RefPtr<Gio::DBus::InterfaceInfo>(),
			Gio::DBus::ProxyFlags::PROXY_FLAGS_DO_NOT_LOAD_PROPERTIES));

	dev_proxy->call("Connect",
			[dev_proxy] (Glib::RefPtr<Gio::AsyncResult>& result) {
			  try
			  {
			    dev_proxy->call_finish(result);
			  }
			  catch (Glib::Exception const & exc)
			  {
			    std::cerr << "Connect failed: " << exc.what()
				      << '\n';
			    loop->quit();
			  }
			});
      });

    loop->run();
  }


  extern "C" int a2dp_start(snd_pcm_ioplug_t *io)
  {
    a2dp_pcm *pcm (static_cast<a2dp_pcm *>(io->private_data));

    (*pcm->start)();

    return 0;
  }

  extern "C" int a2dp_stop(snd_pcm_ioplug_t *io)
  {
    a2dp_pcm *pcm (static_cast<a2dp_pcm *>(io->private_data));

    (*pcm->stop)();

    return 0;
  }

  extern "C" snd_pcm_sframes_t a2dp_pointer(snd_pcm_ioplug_t *io)
  {
    a2dp_pcm *pcm (static_cast<a2dp_pcm *>(io->private_data));
    return pcm->io_ptr % io->buffer_size;
  }

  extern "C" int a2dp_close(snd_pcm_ioplug_t *io)
  {
    a2dp_pcm *pcm (static_cast<a2dp_pcm *>(io->private_data));

    (*pcm->close)();
    if (pcm->t.joinable())
    {
      pcm->t.join();
    }

    return 0;
  }

  extern "C" int a2dp_hw_params(snd_pcm_ioplug_t *io, snd_pcm_hw_params_t *params)
  {
    a2dp_pcm *pcm (static_cast<a2dp_pcm *>(io->private_data));
    return 0;
  }

  extern "C" int a2dp_hw_free(snd_pcm_ioplug_t *io)
  {
    a2dp_pcm *pcm (static_cast<a2dp_pcm *>(io->private_data));
    return 0;
  }

  extern "C" int a2dp_sw_params(snd_pcm_ioplug_t *io, snd_pcm_sw_params_t *params)
  {
    a2dp_pcm *pcm (static_cast<a2dp_pcm *>(io->private_data));
    return 0;
  }

  extern "C" int a2dp_prepare(snd_pcm_ioplug_t *io)
  {
    a2dp_pcm *pcm (static_cast<a2dp_pcm *>(io->private_data));

    pcm->io_ptr = 0u;
    if (!pcm->t.joinable())
    {
      pcm->t = std::thread(consumer, pcm);
      {
        std::unique_lock<std::mutex> l(pcm->sync);
        pcm->cond_var.wait(l, [pcm] () { return pcm->encoder; });
      }
    }

    return 0;
  }

  extern "C" int a2dp_drain(snd_pcm_ioplug_t *io)
  {
    a2dp_pcm *pcm (static_cast<a2dp_pcm *>(io->private_data));

    return 0;
  }

  extern "C" int a2dp_pause(snd_pcm_ioplug_t *io, int enable)
  {
    a2dp_pcm *pcm (static_cast<a2dp_pcm *>(io->private_data));

    return 0;
  }

  extern "C" int a2dp_resume(snd_pcm_ioplug_t *io)
  {
    a2dp_pcm *pcm (static_cast<a2dp_pcm *>(io->private_data));

    return 0;
  }

  extern "C" int a2dp_poll_descriptors_count(snd_pcm_ioplug_t *io)
  {
    a2dp_pcm *pcm (static_cast<a2dp_pcm *>(io->private_data));

    return 1;
  }

  extern "C" int a2dp_poll_descriptors(snd_pcm_ioplug_t *io, struct pollfd *pfd, unsigned int space)
  {
    a2dp_pcm *pcm (static_cast<a2dp_pcm *>(io->private_data));

    pfd[0].fd = pcm->event_fd;
    pfd[0].events = POLLIN;

    return 1;
  }

  extern "C" int a2dp_poll_revents(snd_pcm_ioplug_t *io, struct pollfd *pfd, unsigned int nfds, unsigned short *revents)
  {
    a2dp_pcm *pcm (static_cast<a2dp_pcm *>(io->private_data));

    if (pfd[0].revents & POLLIN)
    {
      eventfd_t event = 0;
      eventfd_read(pcm->event_fd, &event);

      if (event > 0)
      {
        *revents = POLLIN;
      }
    }
    else
    {
      *revents = 0;
    }

    return 0;
  }

  const snd_pcm_ioplug_callback_t a2dp_callback = {
    .start = a2dp_start,
    .stop = a2dp_stop,
    .pointer = a2dp_pointer,
    .transfer = nullptr,
    .close = a2dp_close,
    .hw_params = a2dp_hw_params,
    .hw_free = a2dp_hw_free,
    .sw_params = a2dp_sw_params,
    .prepare = a2dp_prepare,
    .drain = a2dp_drain,
    .pause = a2dp_pause,
    .resume = a2dp_resume,
    .poll_descriptors_count = a2dp_poll_descriptors_count,
    .poll_descriptors = a2dp_poll_descriptors,
    .poll_revents = a2dp_poll_revents
  };

  int a2dp_set_hw_constraint(struct a2dp_pcm *pcm)
  {
    snd_pcm_ioplug_t *io = &pcm->io;

    static const unsigned int accesses[] = {
      SND_PCM_ACCESS_MMAP_INTERLEAVED,
      SND_PCM_ACCESS_RW_INTERLEAVED,
    };
    static const unsigned int formats[] = {
      SND_PCM_FORMAT_S16_LE,
    };

    int err;

    if ((err = snd_pcm_ioplug_set_param_list(io, SND_PCM_IOPLUG_HW_ACCESS,
                2, accesses)) < 0)
      return err;

    if ((err = snd_pcm_ioplug_set_param_list(io, SND_PCM_IOPLUG_HW_FORMAT,
                1, formats)) < 0)
      return err;

    if ((err = snd_pcm_ioplug_set_param_minmax(io, SND_PCM_IOPLUG_HW_PERIODS,
                2, 1024)) < 0)
      return err;

    /* In order to prevent audio tearing and minimize CPU utilization, we're
     * going to setup buffer size constraints. These limits are derived from
     * the transport sampling rate and the number of channels, so the buffer
     * "time" size will be constant. The minimal period size and buffer size
     * are respectively 10 ms and 200 ms. Upper limits are not constraint. */
    unsigned int min_p = 48000 * 20 / 1000 * 2 * 2;
    unsigned int min_b = 48000 * 400 / 1000 * 2 * 2;

    if ((err = snd_pcm_ioplug_set_param_minmax(io, SND_PCM_IOPLUG_HW_PERIOD_BYTES,
                min_p, 1024 * 16)) < 0)
      return err;

    if ((err = snd_pcm_ioplug_set_param_minmax(io, SND_PCM_IOPLUG_HW_BUFFER_BYTES,
                min_b, 1024 * 1024 * 16)) < 0)
      return err;

    if ((err = snd_pcm_ioplug_set_param_minmax(io, SND_PCM_IOPLUG_HW_CHANNELS,
                2, 2)) < 0)
      return err;

    if ((err = snd_pcm_ioplug_set_param_minmax(io, SND_PCM_IOPLUG_HW_RATE,
                48000, 48000)) < 0)
      return err;

    return 0;
  }
}

extern "C"
{
  SND_PCM_PLUGIN_DEFINE_FUNC(a2dp)
  {
    const char *interface = "hci0";
    const char *device = nullptr;
    struct a2dp_pcm *pcm;
    int ret;

    Gio::init();

    snd_config_iterator_t i, next;
    snd_config_for_each(i, next, conf)
    {
      snd_config_t *n = snd_config_iterator_entry(i);

      const char *id;
      if (snd_config_get_id(n, &id) < 0)
      {
        continue;
      }

      if (strcmp(id, "comment") == 0 ||
          strcmp(id, "type") == 0 ||
          strcmp(id, "hint") == 0)
      {
        continue;
      }

      if (strcmp(id, "interface") == 0)
      {
        if (snd_config_get_string(n, &interface) < 0)
        {
          SNDERR("Invalid type for %s", id);
          return -EINVAL;
        }
        continue;
      }
      if (strcmp(id, "device") == 0)
      {
        if (snd_config_get_string(n, &device) < 0)
        {
          SNDERR("Invalid type for %s", id);
          return -EINVAL;
        }
        continue;
      }

      SNDERR("Unknown field %s", id);
      return -EINVAL;
    }

    pcm = new a2dp_pcm{ };

    pcm->interface = interface;
    pcm->device = device;
    pcm->event_fd = eventfd(0, EFD_CLOEXEC);

    pcm->io.version = SND_PCM_IOPLUG_VERSION;
    pcm->io.name = "A2DP";
    pcm->io.flags = SND_PCM_IOPLUG_FLAG_LISTED;
    pcm->io.mmap_rw = 1;
    pcm->io.callback = &a2dp_callback;
    pcm->io.private_data = pcm;

    if ((ret = snd_pcm_ioplug_create(&pcm->io, name, stream, mode)) < 0)
    {
      goto fail;
    }

    if ((ret = a2dp_set_hw_constraint(pcm)) < 0)
    {
      snd_pcm_ioplug_delete(&pcm->io);
      goto fail;
    }

    *pcmp = pcm->io.pcm;
    return 0;

  fail:
    free(pcm);

    return ret;
  }

  SND_PCM_PLUGIN_SYMBOL(a2dp);
}
