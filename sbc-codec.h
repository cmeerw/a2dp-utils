#ifndef A2DP_UTILS__SBC_CODEC__h
#define A2DP_UTILS__SBC_CODEC__h

#include <cstdint>
#include <memory>
#include <utility>

#include <giomm.h>
#include <sbc/sbc.h>

#include "a2dp-codecs.h"


struct SBC_Base
{
  explicit SBC_Base(a2dp_sbc_t conf);

protected:
  sbc_t sbc_;
};

struct SBC_Encoder
  : SBC_Base
{
  SBC_Encoder(a2dp_sbc_t conf, guint16 read_mtu, guint16 write_mtu);

  inline unsigned int mtu() const
  {
    return mtu_;
  }

  unsigned int remaining() const;

  std::pair<unsigned char *, unsigned int> encode(unsigned char *data,
      unsigned int len);

private:
  const guint16 bufsize_;
  const std::unique_ptr<unsigned char[]> buf_;
  const guint16 enc_frame_length_;
  const guint16 enc_bufsize_;
  const std::unique_ptr<unsigned char[]> enc_buf_;

  const guint16 mtu_;
  guint16 bufpos_ = 0;
  uint16_t seq_num_ = 0;
  uint32_t timestamp_ = 0;
};

struct SBC_Decoder
  : SBC_Base
{
  SBC_Decoder(a2dp_sbc_t conf, guint16 read_mtu, guint16 write_mtu);

  std::pair<unsigned char *, unsigned int> decode(unsigned char *data,
      unsigned int len);

protected:
  guint16 bufsize_;
  std::unique_ptr<unsigned char[]> buf_;
};

#endif
