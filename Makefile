DESTDIR?=
INSTALL?=install
prefix?=/usr

CFLAGS := -std=c++14 \
	$(shell pkg-config --cflags alsa) \
	$(shell pkg-config --cflags dbus-1) \
	$(shell pkg-config --cflags glibmm-2.4) \
	$(shell pkg-config --cflags giomm-2.4) \
	$(shell pkg-config --cflags sbc) \
	$(CFLAGS) -O1 -g -DDEBUG -Wall -pthread
LDFLAGS := $(LDFLAGS) -pthread

all: a2dpstream libasound_module_pcm_a2dp.so ondemandpipe

clean:
	-rm -f a2dpstream libasound_module_pcm_a2dp.so

%.o: %.cpp
	$(CXX) $(CFLAGS) -c -fPIC -o $@ $<

a2dpstream: a2dpstream.o sbc-codec.o
	$(CXX) $(CFLAGS) $(LDFLAGS) -o $@ $^ \
		$(shell pkg-config --libs dbus-1) \
		$(shell pkg-config --libs glibmm-2.4) \
		$(shell pkg-config --libs giomm-2.4) \
		$(shell pkg-config --libs sbc)

libasound_module_pcm_a2dp.so: a2dp-alsa.o sbc-codec.o
	$(CXX) $(CFLAGS) $(LDFLAGS) --shared -o $@ $^ \
		$(shell pkg-config --libs alsa) \
		$(shell pkg-config --libs dbus-1) \
		$(shell pkg-config --libs glibmm-2.4) \
		$(shell pkg-config --libs giomm-2.4) \
		$(shell pkg-config --libs sbc)

ondemandpipe: ondemandpipe.o
	$(CXX) $(CFLAGS) $(LDFLAGS) -o $@ $^

install: all
	mkdir -p $(DESTDIR)$(prefix)/bin \
		$(DESTDIR)$(prefix)/lib/$$(uname -m)-linux-gnu/alsa-lib

	$(INSTALL) a2dpstream ondemandpipe $(DESTDIR)$(prefix)/bin/
	$(INSTALL) libasound_module_pcm_a2dp.so $(DESTDIR)$(prefix)/lib/$$(uname -m)-linux-gnu/alsa-lib/

.PHONY: all clean install
