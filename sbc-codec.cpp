/*
 * a2dp-utils, https://cmeerw.org
 * Copyright (C) 2016-2019, Christof Meerwald
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "sbc-codec.h"

#include <algorithm>
#include <cassert>
#include <iostream>

#include <arpa/inet.h>

#include "rtp.h"


SBC_Base::SBC_Base(a2dp_sbc_t conf)
  : sbc_{}
{
  sbc_init_a2dp (&sbc_, 0, &conf, sizeof(conf));
}

SBC_Encoder::SBC_Encoder(a2dp_sbc_t conf, guint16 read_mtu, guint16 write_mtu)
  : SBC_Base(conf),
    bufsize_((
          (sbc_.bitpool = conf.max_bitpool),
          sbc_get_codesize(&sbc_))),
    buf_(new unsigned char[bufsize_]),
    enc_frame_length_(sbc_get_frame_length(&sbc_)),
    enc_bufsize_(write_mtu),
    enc_buf_(new unsigned char[enc_bufsize_]),
    mtu_(((enc_bufsize_ - sizeof (struct rtp_header) - sizeof (struct rtp_payload)) / enc_frame_length_) * bufsize_)
{ }

unsigned int SBC_Encoder::remaining() const
{
  return ((enc_bufsize_ - sizeof (struct rtp_header) - sizeof (struct rtp_payload)) / enc_frame_length_) * bufsize_ - bufpos_;
}

std::pair<unsigned char *, unsigned int>
SBC_Encoder::encode(unsigned char *data, unsigned int len)
{
  assert(len <= remaining());

  if ((bufpos_ != 0u) || (len < bufsize_))
  {
    const unsigned int l = std::min<unsigned int>(len, bufsize_ - bufpos_);
    std::copy(data, data + l, buf_.get() + bufpos_);
    bufpos_ += l;
    data += l;
    len -= l;
  }

  if ((bufpos_ + len) >= bufsize_)
  {
    struct rtp_header * const header = reinterpret_cast<struct rtp_header *>(enc_buf_.get());
    struct rtp_payload * const payload = reinterpret_cast<struct rtp_payload *>((uint8_t*) header + sizeof(*header));

    unsigned char *enc_bufiter = enc_buf_.get() + sizeof(*header) + sizeof(*payload);
    const unsigned char *bufiter = (bufpos_ != 0u) ? buf_.get() : data;

    while (bufpos_ + len >= bufsize_)
    {
      ssize_t written;
      ssize_t encoded = sbc_encode(&sbc_, bufiter, bufsize_,
          enc_bufiter, enc_buf_.get() + enc_bufsize_ - enc_bufiter,
          &written);

      if (encoded != bufsize_)
      {
        std::cerr << "SBC encoding error " << encoded << '\n';
        break;
      }

      if (bufpos_ != 0u)
      {
        bufpos_ = 0u;
        bufiter = data;
      }
      else
      {
        len -= encoded;
        bufiter += encoded;
      }

      enc_bufiter += written;
    }

    std::copy(bufiter, bufiter + len, buf_.get());
    bufpos_ = len;

    // encapsulate it in a2dp RTP packets
    *header = rtp_header();
    header->v = 2;
    header->pt = 1;
    header->sequence_number = htons(seq_num_++);
    header->timestamp = htonl(timestamp_);
    header->ssrc = htonl(1);
    *payload = rtp_payload();
    payload->frame_count = (enc_bufiter - enc_buf_.get() - sizeof(*header) - sizeof(*payload)) / enc_frame_length_;

    timestamp_ += sbc_get_frame_duration(&sbc_) * payload->frame_count;

    return { enc_buf_.get(), enc_bufiter - enc_buf_.get() };
  }
  else
  {
    return { nullptr, 0u };
  }
}


SBC_Decoder::SBC_Decoder(a2dp_sbc_t conf, guint16 read_mtu, guint16 write_mtu)
  : SBC_Base(conf),
    bufsize_()
{ }

std::pair<unsigned char *, unsigned int>
SBC_Decoder::decode(unsigned char *data, unsigned int len)
{
  if (len > (sizeof (struct rtp_header) + sizeof (struct rtp_payload)))
  {
    const struct rtp_payload * const payload = reinterpret_cast<struct rtp_payload *>(data + sizeof(struct rtp_header));
    const size_t required_bufsize = payload->frame_count * sbc_get_codesize(&sbc_);

    if (! buf_ || bufsize_ < required_bufsize)
    {
      bufsize_ = required_bufsize;
      buf_.reset(new unsigned char[bufsize_]);
    }

    unsigned char *bufend = buf_.get();
    const unsigned char *iter = data + sizeof(struct rtp_header) + sizeof(struct rtp_payload);

    for (unsigned i = 0; i != payload->frame_count; ++i)
    {
      size_t written;
      ssize_t decoded = sbc_decode(&sbc_,
          iter, data + len - iter,
          bufend, buf_.get() + bufsize_ - bufend, &written);

      if (decoded <= 0)
      {
        std::cerr << "SBC decoding error " << decoded << '\n';
        break;
      }

      iter += decoded;
      bufend += written;
    }

    return { buf_.get(), bufend - buf_.get() };
  }
  else
  {
    return { nullptr, 0u };
  }
}
