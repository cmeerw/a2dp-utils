/*
 * a2dp-utils, https://cmeerw.org
 * Copyright (C) 2019, Christof Meerwald
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <cstdio>

#include <errno.h>
#include <fcntl.h>
#include <poll.h>
#include <signal.h>
#include <unistd.h>


int main(int argc, char *argv[])
{
  {
    const int flags = fcntl(0, F_GETFL, 0);
    fcntl(0, F_SETFL, flags | O_NONBLOCK);
  }

  {
    struct sigaction act { };
    act.sa_handler = SIG_IGN;
    act.sa_flags = SA_NOCLDWAIT;
    sigaction(SIGCHLD, &act, nullptr);
  }

  {
    struct sigaction act { };
    act.sa_handler = SIG_IGN;
    sigaction(SIGPIPE, &act, nullptr);
  }

  int pipefds[2] = { -1, -1 };

  while (true)
  {
    unsigned char buf[4096];

    struct pollfd pfds[1] = {
      { 0, POLLIN, 0 }
    };

    int rc = poll(pfds, 1, (pipefds[1] == -1) ? -1 : 2000);
    if (rc > 0)
    {
      if (pfds[0].revents & POLLIN)
      {
        const int readlen = read(0, buf, sizeof(buf));
        if (readlen > 0)
        {
          if (pipefds[1] == -1)
          {
            pipe(pipefds);

            pid_t cpid = fork();
            if (cpid == 0)
            {
              close(pipefds[1]);
              dup2(pipefds[0], 0);
              close(pipefds[0]);
              execvp(argv[1], argv + 1);

              return -1;
            }
            else if (cpid != -1)
            {
              close(pipefds[0]);
              pipefds[0] = -1;
            }
            else
            {
              perror("Unable to fork");
            }
          }

          const int writelen = write(pipefds[1], buf, readlen);
          if (writelen != readlen)
          {
            break;
          }
        }
        else
        {
          break;
        }
      }
    }
    else if (rc == 0)
    {
      if (pipefds[1] != -1)
      {
        close(pipefds[1]);
        pipefds[1] = -1;
      }
    }
  }

  if (pipefds[1] != -1)
  {
    close(pipefds[1]);
  }
}
